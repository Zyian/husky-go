package main

import (
	"github.com/bwmarrin/discordgo"
)

type MessageContext struct {
	RawContent string
	Root       string
	Guild      *discordgo.Guild
	Channel    *discordgo.Channel
}
