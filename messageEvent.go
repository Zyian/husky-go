package main

import (
	"strings"

	"github.com/Sirupsen/logrus"

	"github.com/bwmarrin/discordgo"
)

func messageEvent(dSess *discordgo.Session, eMsg *discordgo.Message) {
	var err error
	s := strings.Split(eMsg.Content, " ")

	mC := new(MessageContext)
	mC.Root = strings.Trim(s[0], "!")

	if mC.Channel, err = dSess.Channel(eMsg.ChannelID); err != nil {
		logrus.Errorf("error getting channel %v", err)
	}
	mC.Guild, _ = dSess.Guild(mC.Channel.GuildID)
}
