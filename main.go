package main

import (
	"os"
	"os/signal"

	"github.com/Sirupsen/logrus"

	"github.com/bwmarrin/discordgo"
)

func main() {
	sess, err := discordgo.New("Bot " + os.Getenv(""))
	if err != nil {
		logrus.Panicf("%v", err)
	}

	sess.AddHandler(nil)

	if err := sess.Open(); err != nil {
		logrus.Panicf("%v", err)
	}

	logrus.Info("Discord Running. Exit with CTRL+C")
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, os.Kill)
	if sig := <-sigChan; sig != nil {
		logrus.Infof("Signal Recieved: %v\nClosing...", sig)
	}
}
